const apiKey = '027e0bfe82438de1cd451d719c1e2f5b';
const token ='4a3e8b2b9d7af10b3a979494f427ed2bd2c1b1ec789087368adf56ac2e13250e';
const listId = '5e09047c5ce4f337caa33790';
const addCardBlueButton = document.getElementById('add-card-button');
const addCardGreenButton = document.getElementById('add-card-form');

//@desc-->fetch checklist will fetch the whole checklist and the inner loop will create division's for each items and append it to modal content
//@param-->it's the id of the card to fetch the details of checklist through id of card
async function fetchCheckList(idOfCard) {
  await fetch(`
 https://api.trello.com/1/cards/${idOfCard}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=${apiKey}&token=${token}
 `)
    .then(response => response.json())
    .then(checkLists => {
      checkLists.forEach(data => {
        let modalContent = document.getElementById('modal-card-content');
        let checkListNameHeader = document.createElement('div');
        checkListNameHeader.id = data.id;
        checkListNameHeader.classList.add('target');
        checkListNameHeader.innerHTML = `<h3 class="target ${data.name}" id="list-header">${data.name}</h3>
        <button class="btn btn-secondary btn-sm p-1 delete-checklist target ${data.name}" id=${data.id}>X</button>
        <hr/>
        <div id="checklist-header">
        <button class="btn btn-primary btn-sm target" id="add-items-button"style="margin-top: 0.5vw;">
        Add items
        </button>
        <div class="add-card target"id="add-items-form"style="display:none;">
        <button class="btn btn-success target"id="create-items-button">
          Create items
        </button>
        <input class="form-control form-control-lg mb-2 target" id="items-name"type="text"placeholder="Add an item"/>
        <span>
          <button class="btn btn-secondary target"id="close-items-form">
            X
          </button>
        </span>
      </div>
    </div>`;
        //modal contents are updated over here
        modalContent.appendChild(checkListNameHeader);
        data.checkItems.forEach(items => {
          let innerContentOfModal = document.createElement('div'); //creating the division for checkList
          innerContentOfModal.id = data.id; //giving the checkList Id
          //checking if the state is complete or incomplete
          let checkedValue = 'unchecked';
          if (items.state === 'complete') {
            checkedValue = 'checked';
          }
          innerContentOfModal.innerHTML = `<div class="checkbox target" id=${idOfCard}>
      <label class="target"><input class="target" id=${items.id} type="checkbox" value="" ${checkedValue}>${items.name}</label>
      <button class="btn btn-secondary btn-sm p-1 delete-checklist-item target" id=${items.id}>X</button>
      </div>`;
          modalContent.appendChild(innerContentOfModal);
        });
      });
    });
  //adding footer for each card
  const modalFooter = document.getElementById('modal-footer');
  const addChecklistDivision = document.createElement('div');
  addChecklistDivision.id = idOfCard;
  addChecklistDivision.classList.add('target');
  addChecklistDivision.innerHTML = `<button class="btn btn-primary btn-sm target add-checklist-button" id="add-checklist-btn" style="margin-top: 0.5vw;">
  Add Checklist
</button>
<div class="target" id="add-checklist-form" style="display:none">
  <input class="form-control form-control-lg mb-2 target" id="checklist-name" type="text" placeholder="Enter checklist name...."/>
  <button class="btn btn-success target create-checklist-button" id="create-checklist-button">
    Create Checklist
  </button>
  <span>
    <button class="btn btn-secondary target" id="close-checklist-button">
      X
    </button>
  </span>
  </div>`;

  modalFooter.appendChild(addChecklistDivision);
}

function checkItems(itemId, cardId, state) {
  if (state == true) {
    state = 'complete';
  } else {
    state = 'incomplete';
  }
  fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${state}&key=${apiKey}&token=${token}`,
    { method: 'PUT' }
  );
}
function addChecklist(idOfCard) {
  //making invisible add checklist button
  const addChecklistButton = document.getElementById('add-checklist-btn');

  const createChecklistButton = document.getElementById('add-checklist-form');

  addChecklistButton.style.display = 'none';
  createChecklistButton.style.display = 'inline';
  //getting name of checklist
  document
    .getElementById('create-checklist-button')
    .addEventListener('click', () => {
      const checkListName = document.getElementById('checklist-name').value;
      //posting the new checklist to original trello
      fetch(
        `https://api.trello.com/1/cards/5e0c837b3b7d4b6fe35e2346/checklists?name=${checkListName}&key=${apiKey}&token=${token}`,
        { method: 'POST' }
      )
        .then(response => response.json())
        .then(checklist => {
          addChecklistButton.style.display = 'inline';
          createChecklistButton.style.display = 'none';

          //adding the new checklist to the modal content
          let modalContent = document.getElementById('modal-card-content');
          let checkListNameHeader = document.createElement('div');
          checkListNameHeader.id = checklist.id;
          checkListNameHeader.classList.add('target');
          checkListNameHeader.innerHTML = `<h3 class="target ${checklist.name}" id="list-header">${checklist.name}</h3>
          <button class="btn btn-secondary btn-sm p-1 delete-checklist target ${checklist.name}" id=${checklist.id}>X</button>
          <hr/>
          <div id="checklist-header">
          <button class="btn btn-primary btn-sm target" id="add-items-button"style="margin-top: 0.5vw;">
          Add items
          </button>
          <div class="add-card target"id="add-items-form"style="display:none;">
          <button class="btn btn-success target"id="create-items-button">
            Create items
          </button>
          <input class="form-control form-control-lg mb-2 target" id="items-name"type="text"placeholder="Add an item"/>
          <span>
            <button class="btn btn-secondary target"id="close-items-form">
              X
            </button>
          </span>
        </div>
      </div>`;
          //modal contents are updated over here
          modalContent.appendChild(checkListNameHeader);
        });
    });

  //if 'close-checklist-button' is clicked the 'add-checklist-btn reappears'
  document
    .getElementById('close-checklist-button')
    .addEventListener('click', () => {
      addChecklistButton.style.display = 'inline';
      createChecklistButton.style.display = 'none';
    });
}

function deleteCheklistItem(event) {
  const itemId = event.target.id;
  const checkListId = event.target.parentElement.id;
  const itemDivision = event.target.parentElement.parentElement;
  fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems/${itemId}?key=${apiKey}&token=${token}`,
    { method: 'DELETE' }
  );

  const modalContent = document.getElementById('modal-card-content');
  modalContent.removeChild(itemDivision);
}

function addingCards(name, idOfCard) {
  //creating division to add card
  let cardDivision = document.createElement('div');

  cardDivision.id = idOfCard;
  //adding the name of the class to the class list
  cardDivision.classList.add('card');

  //intializing division inside the innerHTML
  cardDivision.innerHTML = `<div class="card-body d-flex justify-content-between align-items-center: 4vw" data-toggle="modal" data-target="#myModal"><h6 class="card-title" font-weight-bold mb-0>${name}</h6><button class="btn btn-secondary btn-sm p-1 delete" id="delete-card-button">X</button></div>`;
  const list = document.getElementById('list-cards');
  list.appendChild(cardDivision);
}

function makeVisibleCreateCardButton() {
  //checking if the add card button is clicked

  if (event.target.id === 'add-card-button') {
    //if clicked! the blue button is hidden
    addCardBlueButton.style.display = 'none';
    //after the blue button is clicked the green button appears
    addCardGreenButton.style.display = 'inline';
  }
}
function makeVisibleCreateItemsButton(
  additemsBlueButton,
  createitemsGreenButton
) {
  additemsBlueButton.style.display = 'none';
  createitemsGreenButton.style.display = 'inline';
}
function makeVisibleAddItemsButton(additemsBlueButton, createitemsGreenButton) {
  additemsBlueButton.style.display = 'inline';

  createitemsGreenButton.style.display = 'none';
}
function makeVisibleAddCardButton() {
  addCardBlueButton.style.display = 'inline';
  addCardGreenButton.style.display = 'none';
}

//////////////////////////////////////////////////////////////////////getting card from the trello account
async function fetchCard() {
  //fetching cards from the trello api

  await fetch(
    `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${token}`
  )
    .then(response => response.json())
    .then(cards => {
      cards.forEach(card => {
        addingCards(card.name, card.id);
      });
    });
}
fetchCard();
////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////post request of card
async function createCard() {
  makeVisibleCreateCardButton();

  document
    .getElementById('create-card-button')
    .addEventListener('click', async () => {
      //from the text box extracting name

      const nameOfCard = document.getElementById('card-name').value;
      //sending the name of the card to create card
      await fetch(
        `https://api.trello.com/1/cards?name=${nameOfCard}&idList=5e09047c5ce4f337caa33790&keepFromSource=all&key=${apiKey}&token=${token}`,
        { method: 'POST' }
      )
        .then(response => {
          return response.json();
        })
        .then(response => {
          addingCards(nameOfCard, response.id);
        });

      //the card is added in my own trello api
    });

  document.getElementById('close-form').addEventListener('click', () => {
    makeVisibleAddCardButton();
  });
}
async function createChecklistItems(event) {
  const additemsBlueButton = document.getElementById('add-items-button');
  const createitemsGreenButton = document.getElementById('add-items-form');
  makeVisibleCreateItemsButton(additemsBlueButton, createitemsGreenButton);
  document
    .getElementById('create-items-button')
    .addEventListener('click', async event => {
      const nameOfItem = document.getElementById('items-name').value;
      await fetch(
        `https://api.trello.com/1/checklists/${event.path[3].id}/checkItems?name=${nameOfItem}&pos=bottom&checked=false&key=${apiKey}&token=${token}`,
        { method: 'POST' }
      )
        .then(response => {
          return response.json();
        })
        .then(data => {
          //update the modal content here
          let modalContent = document.getElementById('modal-card-content');
          let innerContentOfModal = document.createElement('div'); //creating the division for checkList
          innerContentOfModal.id = data.idChecklist; //giving the checkList Id
          //checking if the state is complete or incomplete
          innerContentOfModal.innerHTML = `<div class="checkbox target" id=${data.idChecklist}>
          <label class="target"><input class="target" id=${data.id} type="checkbox" value="">${data.name}</label>
          <button class="btn btn-secondary btn-sm p-1 delete-checklist-item target" id=${data.id}>X</button>
          </div> `;

          modalContent.appendChild(innerContentOfModal);

          //
        });

      makeVisibleAddItemsButton(additemsBlueButton, createitemsGreenButton);
    });
  document.getElementById('close-items-form').addEventListener('click', () => {
    makeVisibleAddItemsButton(additemsBlueButton, createitemsGreenButton);
  });
}

function deleteCard(event) {
  //deleting
  const id = event.target.parentElement.parentElement.id;
  fetch(`https://api.trello.com/1/cards/${id}?key=${apiKey}&token=${token}`, {
    method: 'delete'
  }).catch(error => {
    console.log(error);
  });

  const list = document.getElementById('list-cards');
  let li = event.target.parentElement.parentElement;
  list.removeChild(li);
}
function deleteChecklist(checklistId) {
  fetch(
    `https://api.trello.com/1/checklists/${checklistId}?key=${apiKey}&token=${token}`,
    {
      method: 'delete'
    }
  ).catch(error => {
    console.log(error);
  });
  const modalChild = document.getElementById(checklistId);

  const modalContent = document.getElementById('modal-card-content');
  modalContent.removeChild(modalChild);
}
document
  .getElementById('add-card-button')
  .addEventListener('click', createCard);

///////////////////////////////////////////////////////////////////////////////////////////////////////

//for deleting the card from the list
document.getElementById('list-cards').addEventListener('click', event => {
  if (event.target.classList.contains('delete')) {
    deleteCard(event);
  } 
  else {
    //sending the id of card which is the division id in which the card is in
    fetchCheckList(event.target.offsetParent.id);
  }
});
//removing the modal content if it is clicked anywhere in the screen
document.addEventListener('click', event => {
  let modalContent = document.getElementById('modal-card-content');
  let modalFooter = document.getElementById('modal-footer');
  if (!event.target.classList.contains('target')) {
    while (modalContent.hasChildNodes()) {
      modalContent.removeChild(modalContent.firstChild);
    }
    modalFooter.removeChild(modalFooter.firstElementChild);
  } else if (event.target.id === 'add-items-button') {
    createChecklistItems(event);
  } else if (event.target.classList.contains('delete-checklist')) {
    deleteChecklist(event.target.id);
  } else if (event.target.classList.contains('delete-checklist-item')) {
    deleteCheklistItem(event);
  } else if (event.target.classList.contains('add-checklist-button')) {
    addChecklist(event.target.id);
  } else if (event.path[2].classList.contains('checkbox')) {
    //sending the item's id and card id
    checkItems(event.path[0].id, event.path[2].id, event.path[0].checked);
  }
});
